# r-devbox

Personal R development containers. This is a hobby and learning project of highly-opinionated and changeable configurations.

## Premise

Containers are the best way to manage R projects into production, allowing versioning of all components, as well as portability to any host OS (Mac, Windows, Linux) or host infrastructure (PC, VM, K8S, etc)

Having a standard R development box will help with rapidly containerising workloads as well as make my development container-first.

## Development plan

- Create R images with standard set of packages and configured for further project-specific development (base images)
  - Choose a linux OS (debian vs ubuntu vs CentOS)
  - Set up R building and installation with appropriate dependencies
  - Choose package management tools ({renv} vs {pak})
  - Finalise so can run as general, scrappable environments, or for further project-specific building
- Scope possibility of creating S2I images for building on OpenShift (RedHat OCP K8S)
- Automate building new images for different R versions
- Microservice app for launching different R version dev environments

## Development notes

### Commands to remember

Build and push new image to registry

``` bash
docker build -t registry.gitlab.com/isaac-florence/r-devbox .
docker push registry.gitlab.com/isaac-florence/r-devbox
```

### Requirements

- Docker
- Git
- GitLab [isaac-florence/r-devbox](https://gitlab.com/isaac-florence/r-devbox/) permissions

### Recommendations

- VS Code with the following extensions
  - Docker
  - Error lens
  - Git Graph
  - Path Intellisense
  - R
  - Remote - Containers
- GitHub Desktop
